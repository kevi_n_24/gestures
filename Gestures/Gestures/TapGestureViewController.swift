//
//  TapGestureViewController.swift
//  Gestures
//
//  Created by Kevin on 17/1/18.
//  Copyright © 2018 Kevin. All rights reserved.
//

import UIKit

class TapGestureViewController: UIViewController {
    
    
    
    
    @IBOutlet weak var touchesLabel: UILabel!
    
    @IBOutlet weak var tapsLabel: UILabel!
    
    @IBOutlet weak var coordLabel: UILabel!
    
    @IBOutlet weak var viewImageToch: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touchCount = touches.count
        let tap = touches.first
        let tapCount = tap?.tapCount
        
        touchesLabel.text = "\(touchCount ?? 0)"
        tapsLabel.text = "\(tapCount ?? 0)"
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        let point = touch?.location(in: self.view)
        
        let x = point?.x
        let y = point?.y
        
        print("x: \(x), y: \(y)")
        coordLabel.text = "x: \(x ?? 0), y: \(y ?? 0)"
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Termino :v")
    }
    
    @IBAction func tapGestureAction(_ sender: Any) {
        // controla el tap por un tiempo
        
        //let action = sender as! UITapGestureRecognizer
        
        viewImageToch.backgroundColor = .green
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
