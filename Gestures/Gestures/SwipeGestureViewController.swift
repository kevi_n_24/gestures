//
//  SwipeGestureViewController.swift
//  Gestures
//
//  Created by Kevin on 17/1/18.
//  Copyright © 2018 Kevin. All rights reserved.
//

import UIKit

class SwipeGestureViewController: UIViewController {

    @IBOutlet weak var customView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func swipeUp(_ sender: Any) {
        
        customView.backgroundColor = .red
        
    }
    
    @IBAction func swipeDown(_ sender: Any) {
        
        customView.backgroundColor = .yellow
        
    }
    
    @IBAction func swipeLeft(_ sender: Any) {
        
        customView.backgroundColor = .blue
        
    }
    
    @IBAction func swipeRight(_ sender: Any) {
        
        customView.backgroundColor = .green
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
