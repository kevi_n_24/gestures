//
//  StoryBoardsTabBarController.swift
//  Gestures
//
//  Created by Kevin on 23/1/18.
//  Copyright © 2018 Kevin. All rights reserved.
//

import UIKit

class StoryBoardsTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    override func viewDidAppear(_ animated: Bool) {
        let left = tabBar.items![0]
        let rigth = tabBar.items![1]
        
        rigth.image = #imageLiteral(resourceName: "ic_android")
        left.image = #imageLiteral(resourceName: "ic_android")
        
        left.title = "tap"
        rigth.title = "swipe"
        
        
    }
    
}
